const Schema = require('mongoose').Schema
const app = require('@/app')

const priceDisplay = app.helpers.amount.display

const Model = module.exports = new Schema({
  order: {
    type: Schema.Types.ObjectId,
    ref: 'order'
  },

  product: {
    type: Schema.Types.ObjectId,
    ref: 'product'
  },

  price: {
    amount: {
      type: Number,
      default: 0
    },
    display: {
      type: String,
      default: null
    }
  },

  supplier: {
    type: Schema.Types.ObjectId,
    ref: 'company'
  },

  charges: [{
    chargedAt: Date,
    success: Boolean,
    failureReason: String
  }]
})

Model.pre('save', function(){
  // Add a display amount to price
  this.price.display = priceDisplay(this.price.amount)
  this.charges.push({
    chargedAt: new Date(),
    success: true
  })
})