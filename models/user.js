const Schema = require('mongoose').Schema

const _ = require('lodash')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const Model = module.exports = new Schema({
  name: {
    type: String,
    required: true
  },

  email: {
    type: String,
    required: true
  },

  password: {
    type: String,
    required: true
  },

  companyIds: {
    type: [String]
  },

  active: {
    type: Boolean,
    default: true
  }
})

Model.virtual('company', {
  ref: 'company',
  localField: 'companyIds',
  foreignField: '_id',
  justOne: false
})

Model.method('generateJWT', function () {
  return jwt.sign(_.pick(this, [
    'name',
    'email',
    '_id',
    'permissions',
    'confirmed',
    'cpf',
    ]), app.config.JWT_SECRET)
})


Model.method('comparePassword', function (password) {
  if(this.password == null) return false

  return bcrypt.compareSync(password, this.password)
})

Model.method('encryptPassword', function () {
  return this.constructor.encryptPassword(this.password)
})

Model.static('encryptPassword', (password) => {
  if (!_.isString(password)) {
    password = String(password)
  }
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null)
})


Model.pre('save', async function () {
  if(this.isNew || this.isModified('password')) {
    this.password = this.encryptPassword()
  }
})

