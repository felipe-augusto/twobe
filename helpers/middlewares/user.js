const app = require('@/app')
const errors = require('@/errors')
const unless = require('express-unless')

module.exports = async function getUser(req, res, next) {
  // MOCK an authenticated user
  req.user = await app.models.user.findOne({}).populate('company')
  next()
}

module.exports.unless = unless
