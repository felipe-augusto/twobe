const app = require('@/app')

module.exports = async(router) => {
  router.post('/auth/register',
    app.helpers.routes.func(require('./func.js')))
}
