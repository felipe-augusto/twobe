const _ = require('lodash')
const path = require('path')
const app = require('../../app')

module.exports = async function populate() {
  if(process.argv[2]) {
    let database = process.argv[2]
    process.env.MONGO_URI = `mongodb://localhost:27017/twobe-${database}`
  }

  if(!app.helpers) await app.bootstrap(['config', 'helpers', 'mongo', 'models'])

  const fixtures = new app.helpers.fixtures({
    dir: path.join(process.cwd(), './fixtures'),
    mute: true,
    db: app.mongo,
    order: [
      'categories',
      'measure',
      'product',
      'company',
      'user'
    ]
  })

  await fixtures.unload()
  const loaded = await fixtures.load()
  return loaded.reduce((obj, model) => _.extend(obj, model), {})
}

// make this runnable via terminal
require('make-runnable')
