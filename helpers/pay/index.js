const app = require('@/app')
const createPaymentInstrument = require('./createPaymentInstrument.js')
const createPayment = require('./createPayment.js')

module.exports = async function pay(order, card) {
  const paymentInstrument = await createPaymentInstrument(order, card)
  return await createPayment(order, paymentInstrument.id)
}