const Schema = require('mongoose').Schema

const Model = module.exports = new Schema({
  // Company name
  name: {
    type: String,
    required: true,
  },

  // Financial responsible for this company
  responsible: {
    firstName: {
      type: String,
      required: true
    },
    lastName: {
      type: String,
      required: true
    },
    email: {
      type: String,
      required: true
    },
  },

  phone: {
    type: String
  },

  // Company address
  address: {
    code: {
      type: String,
      default: null,
    },
    state: {
      type: String,
      default: null,
    },
    city: {
      type: String,
      default: null,
    },
    district: {
      type: String,
      default: null,
    },
    address: {
      type: String,
      default: null,
    },
    address2: {
      type: String,
      default: null,
    },
    number: {
      type: String,
      default: null,
    },
    default: {},
  },

  // Company type
  type: {
    type: String,
    enum: ['buyer', 'supplier'],
  },

  // Products that this company is linked to
  productIds: {
    type: [String]
  }
})

Model.virtual('products', {
  ref: 'product',
  localField: 'productIds',
  foreignField: '_id',
  justOne: false
})