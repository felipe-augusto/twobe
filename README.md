# [Twobe]('http://twobe.felipecoder.com')

Where buyers and suppliers make it happen

## Installation

```
yarn install
```

## What it does

You will be able to use Twobe API. Create, read, update and delete orders, products, companys and users. All API response will be in a `JSON format`.

## Usage

### In your terminal:

```
yarn start:watch
```
So your server is been running at `localhost:8011/v1/`

Test your API status in `localhost:8011/v1/status`

### Server feedback

All requests will be logged in this format

```
[server] [2019-02-09T02:53:59.355Z] 200 13.65 ms GET /v1/status ::1
```

### Using docs

To the view the docs, enter: [https://twobe.docs.apiary.io](https://twobe.docs.apiary.io)

### Creator
[Felipe Tiozo](https://gitlab.com/felipetiozo)
[Felipe Augusto](https://gitlab.com/felipe-augusto)