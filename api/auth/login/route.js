const app = require('@/app')

module.exports = async(router) => {
  router.post('/auth/login',
    app.helpers.routes.func(require('./func.js')))
}