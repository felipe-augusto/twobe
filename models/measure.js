const Schema = require('mongoose').Schema

const Model = module.exports = new Schema({
  name: {
    type: String,
    required: true
  }
})