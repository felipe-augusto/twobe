const app = require('@/app')
const restify = require('express-restify-mongoose')

restify.serve(app.router, app.models.product, {
  prefix: '',
  version: '',
  access(req) {
    return 'protected'
  },
  writeAccess(req) {
    return 'protected'
  },
  lean: { virtuals: true },
  totalCountHeader: true,
  runValidators: true,
  preRead: [app.helpers.rest.paginate],
  outputFn: app.helpers.rest.outputFn
})
