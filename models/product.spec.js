const app = require('@/app')
const assert= require('assert')

describe('models /product', async function(){
  var models
  beforeEach(async function(){
    models = await app.helpers.fixtures.populate()
  })
  it('return all categories from product', async function() {
    let product =  await app.models.product.findOne({ _id: models.product[0]._id}).populate('categories')
    assert(product)
    assert.equal(product.categories[0].name, models.categories[0].name)
  })
})