module.exports = [
  {
    _id: '000000000000000000000001',
    name: 'Areia',
    description: 'Some description',
    categoriesIds: ['000000000000000000000001'],
    measure: {
      kind: 'Kilograma(s)',
      quantity: '1'
    },
    price: {
      amount: 10.00
    }
  },
  {
    _id: '000000000000000000000002',
    name: 'Tijolo',
    description: 'Some description',
    categoriesIds: ['000000000000000000000001'],
    measure: {
      kind: 'Unidade(s)',
      quantity: '50'
    },
    price: {
      amount: 100.00
    }
  },
  {
    _id: '000000000000000000000003',
    name: 'Cimento',
    description: 'Some description',
    categoriesIds: ['000000000000000000000001'],
    measure: {
      kind: 'Kilograma(s)',
      quantity: '10'
    },
    price: {
      amount: 100.00
    }
  },
  {
    _id: '000000000000000000000004',
    name: 'Alicate de corte',
    description: 'Some description',
    categoriesIds: ['000000000000000000000002'],
    measure: {
      kind: 'Unidade(s)',
      quantity: '1'
    },
    price: {
      amount: 100.00
    }
  },
  {
    _id: '000000000000000000000005',
    name: 'Chave de fenda',
    description: 'Some description',
    categoriesIds: ['000000000000000000000002'],
    measure: {
      kind: 'Unidade(s)',
      quantity: '1'
    },
    price: {
      amount: 100.00
    }
  },
]