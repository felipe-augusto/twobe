const _ = require('lodash')
const assert = require('assert')
const fs = require('fs')
const path = require('path')
const mongoose = require('mongoose')
const util = require('util')

const readdir = util.promisify(fs.readdir);
const readFile = util.promisify(fs.readFile);
const BATCH_SIZE = 1000;

module.exports = Fixtures;

function orderFiles(files, order) {
  order = order.map(o => o + '.js')
  return _.sortBy(files, (item) => order.indexOf(item) >= 0 ? order.indexOf(item) : 1000)
}

function log(mute, ...msg) {
  if (!mute) console.log(...msg);
}

async function createFixtures (dataset, db) {
  if (typeof db === 'function') {
    callback = db;
    db = mongoose;
  }

  let promises = Object.keys(dataset).map(async (tableName) => {
    var model = db.model(tableName);

    if (model) {
      return await model.create(dataset[tableName])
    } else {
      throw new Error(tableName + ' does not exist');
    }
  })

  return await Promise.all(promises)
}

async function resetFixtures (modelName, db) {
    var model = db.model(modelName);

    if (model) {
        return await model.remove()
    } else {
        throw new Error(modelName + ' does not exist');
    }
};


function Fixtures(options) {
  options = options || {};
  this._mute = options.mute || false;
  this._dir = options.dir || 'fixtures';
  this._match = options.filter ? new RegExp(options.filter) : null;
  log(this._mute, '[info ] Using fixtures directory: ' + this._dir);
  if (this._match === null) log(this._mute, '[info ] No filtering in use');
  this._scripts = [];
  this._db = options.db || mongoose
  this.order = options.order || []
}

Fixtures.prototype.load = async function() {
  const files = orderFiles(await readdir(this._dir), this.order)

  const promises = files
    .filter(file => {
      if (this._match === null) return true;
      const matched = this._match.test(file);
      log(this._mute, '[info ] Filter "' + this._match.source + '" ' + (matched ? 'matches' : 'excludes') + ' fixture: ' + file);
      return matched;
    })
    .map(async file => {
      const parse = path.parse(file);
      const ext = parse.ext.toLowerCase();
      const collectionName = parse.name;

      if (!isSupportedExt(ext)) {
        return null;
      } else if (isScript(collectionName, ext)) {
        const filePath = path.join(this._dir, file);
        return this._scripts.push(filePath);
      }

      const docs = await readCollectionFromJsJson(path.join(this._dir, file))
        .then(docs => {
          if (!Array.isArray(docs)) {
            throw new Error(
              '[error] no docs returned from file: "' +
                file +
                '". verify that a docs array was exported. note: if using .js files, use "module.exports", instead of "exports".'
            );
          }

          return createFixtures({ [collectionName]: docs }, this._db)
        });

      return { [collectionName]: docs[0] }
    });

  return Promise.all(promises)
};

Fixtures.prototype.unload = async function() {
  const files = orderFiles(await readdir(this._dir), this.order)

  const promises = files
    .filter(file => {
      if (this._match === null) return true;
      const matched = this._match.test(file);
      log(this._mute, '[info ] Filter "' + this._match.source + '" ' + (matched ? 'matches' : 'excludes') + ' fixture: ' + file);
      return matched;
    })
    .map(file => {
      const parse = path.parse(file);
      const collectionName = parse.name;
      const ext = parse.ext;

      if (!isSupportedExt(ext) || isScript(collectionName, ext)) {
        return null;
      }

      const filePath = path.join(this._dir, file);
      log(this._mute, '[start] unload', collectionName);
      return resetFixtures(collectionName, this._db)
    });

  return Promise.all(promises).then(() => this);
};

function readCollectionFromJsJson(file) {
  const parse = path.parse(file);
  const collectionName = parse.name;
  const fileExt = parse.ext.toLowerCase();
  // TODO use streams
  if (['.js', '.ts'].indexOf(fileExt) > -1 && fileExt.length === 3) {
    if (!path.isAbsolute(file)) {
      file = path.join(process.cwd(), file);
    }
    const docs = require(file);
    return Promise.resolve(docs);
  } else if (fileExt === '.json') {
    return readFile(file).then(contents => {
      let docs = [];
      try {
        return JSON.parse(contents);
      } catch (e) {
        throw new Error(
          '[error] ' + collectionName + ' in ' + file + ': ' + e.message
        );
      }
    });
  }
}

function isSupportedExt(ext) {
  return ['.json', '.ts', '.js'].indexOf(ext) > -1;
}

function isScript(name, ext) {
  return name.endsWith('_') && ['.ts', '.js'].indexOf(ext) > -1;
}
