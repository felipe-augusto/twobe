const _ = require('lodash')
const app = require('@/app')
const errors = require('@/errors')

module.exports = async(context) => {
  let { user, company } = context.body

  // create company
  company = await app.models.company.create(company)

  // associate company with this user
  user.companyIds = [company.id]

  // create user
  user = await app.models.user.create(user)

  return { token : user.generateJWT() }
}
