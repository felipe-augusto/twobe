const SEVEN_DAYS = 1000 * 60 * 60 * 24 * 7
const FIVE_DAYS = 1000 * 60 * 60 * 24 * 5

module.exports = [{
  _id: '000000000000000000000001',
  name: 'Casa do Seu João',
  dueDate: new Date() + SEVEN_DAYS,
  finished: false,
  company: '000000000000000000000001',
  products: [{
    product: '000000000000000000000001',
    quantity: 5,
    supplier: '000000000000000000000001',
    price: {
      amount: 100
    },
    proposals: [{
      company: '000000000000000000000001',
      price: {
        amount: 100
      }
    }]
  },
  {
    products: '000000000000000000000002',
    quantity: 4,
    supplier: '000000000000000000000001',
    price: {
      amount: 50
    },
    proposals: [{
      company: '000000000000000000000001',
      price: {
        amount: 50
      }
    }]
  }]
},{
  _id: '000000000000000000000002',
  name: 'Carro do seu Judas',
  dueDate: new Date() + FIVE_DAYS,
  finished: false,
  company: '000000000000000000000002',
  products: [{
    product: '000000000000000000000004',
    quantity: 5,
    supplier: '000000000000000000000001',
    price: {
      amount: 350
    },
    proposals: [{
      company: '000000000000000000000002',
      price: {
        amount: 350
      }
    }]
  },
  {
    products: '000000000000000000000005',
    quantity: 4,
    supplier: '000000000000000000000002',
    price: {
      amount: 475
    },
    proposals: [{
      company: '000000000000000000000002',
      price: {
        amount: 475
      }
    }]
  }]
}]