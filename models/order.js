const Schema = require('mongoose').Schema
const app = require('@/app')

const priceDisplay = app.helpers.amount.display

const Model = module.exports = new Schema({
  name: {
    type: String,
  },

  dueDate: {
    type: Date,
    required: true
  },

  // indicates if this order is finished
  finished: {
    type: Boolean,
    default: false
  },

  // hold payment data, after this order has a payment attempt
  payment: {
    id: {
      type: String,
      writeAcess: 'private'
    },
    status: {
      type: String,
      writeAcess: 'private'
    },
  },

  products: [{
    product: {
      type: Schema.Types.ObjectId,
      ref: 'product'
    },

    quantity: Number,

    supplier: {
      type: Schema.Types.ObjectId,
      ref: 'company',
    },

    price: {
      amount: {
        type: Number,
        default: 0
      },
      display: {
        type: String,
        default: null
      }
    },

    proposals: [{
      company: {
        type: Schema.Types.ObjectId,
        ref: 'company'
      },
      price: {
        amount: {
          type: Number,
          default: 0
        },
        display: {
          type: String,
          default: null
        }
      }
    }]
  }],

  company: {
    type: Schema.Types.ObjectId,
    ref: 'company'
  },

  total: {
    amount: {
      type: Number,
      default: 0
    },
    display: {
      type: String,
      default: null
    }
  }
})

Model.virtual('isExpired').get(function(){
  return this.dueDate && this.dueDate > new Date()
})

Model.virtual('isClosed').get(function(){
  return this.finished || this.isExpired
})

Model.pre('save', async function(){
  // Add a display amount to all products price and proposals price
  this.products.map((product) => {
    product.price.display = priceDisplay(product.price.amount)
    product.proposals.map((proposal) => {
      proposal.price.display = priceDisplay(proposal.price.amount)
    })
  })

  // Set total
  let total = 0
  this.products.map((product) => {
    total = total + product.price.amount * product.quantity
    this.total.amount = Number(total)
  })

  // Add a display amount to total
  this.total.display = priceDisplay(Number(this.total.amount))

  if(this.finished) {
    await this.generateInvoices()
  }
})

// Generate invoice to all suppliers to be paid sometime in the future
Model.method('generateInvoices', async function() {
  await Promise.all(this.products.map(async (product) => {
    let invoice = new app.models.invoice({
      order: this._id,
      product: product.product,
      price: {
        amount: product.price.amount
      },
      supplier: product.supplier
    })
    await invoice.save()
  }))
})

// When ann products have suppliers, our order is ready to be paid
Model.virtual('isReady').get(function(){
  return this.products.every((p) => p.supplier) && !this.isExpired
})

Model.virtual('dueDateSeconds').get(function(){
  return (Date.now() - this.dueDate) / 1000
})