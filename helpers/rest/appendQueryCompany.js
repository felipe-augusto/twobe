module.exports = function appendQueryAuthor(req, res, next) {
  req.erm.query.query.company = req.user.companyIds

  // populate by default
  req.erm.query.populate = [
    { path: 'products.product' },
    { path: 'products.supplier' },
    { path: 'products.proposals.company' }
  ]

  // only show orders that are not finished
  req.erm.query.finished = false
  next()
}