module.exports = [
  {
    _id: '000000000000000000000001',
    name: 'Areias April',
    type: 'supplier',
    responsible: {
      firstName: "Felipe",
      lastName: "santos",
      email: "felipe.augusto@tenda.digital"
    },
    productIds: ['000000000000000000000001', '000000000000000000000002', '000000000000000000000003'],
  },
  {
    _id: '000000000000000000000002',
    name: 'Mecânica do Tchela',
    responsible: {
      firstName: "Felipe",
      lastName: "Oliveira",
      email: "felipe.osilva@tenda.digital"
    },
    type: 'buyer',
    productIds: ['000000000000000000000005', '000000000000000000000004'],
  }
]