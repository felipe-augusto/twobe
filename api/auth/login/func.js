const _ = require('lodash')
const app = require('@/app')
const errors = require('@/errors')

module.exports = async(context) => {
  let { email, password } = context.body

  if(!email || !password) {
    throw new errors.BadRequest.MissingParameter('users.missing_login')
  }

  let user =  await app.models.users.findOne({ email: email })

  if(!user) {
    throw new errors.BadRequest.InvalidParameter('users.user_or_password')
  }

  let isValid = user.comparePassword(password)

  if(!isValid) {
    throw new errors.BadRequest.InvalidParameter('users.user_or_password')
  }

  return { token : user.generateJWT() }
}
