const _ = require('lodash')
const app = require('@/app')
const cybersourceRestApi = require('cybersource-rest-client')

module.exports = async function createPaymentInstrument(order, card){
  const instance = new cybersourceRestApi.PaymentInstrumentsApi(app.config.cyberSource.config)
  const body2 = new cybersourceRestApi.Body2()

  // add card
  // const card = new cybersourceRestApi.Tmsv1paymentinstrumentsCard()
  // card.expirationYear = card.expirationYear
  // card.expirationMonth = card.expirationMonth
  card.type = 'visa'

  const billTo = new cybersourceRestApi.Tmsv1paymentinstrumentsBillTo()
  // create bill of responsible
  billTo.firstName = order.company.responsible.firstName
  billTo.lastName = order.company.responsible.lastName
  billTo.email = order.company.responsible.email

  billTo.phoneNumber = order.company.phone
  billTo.company = order.company.name
  billTo.address1 = order.company.address.address
  billTo.address2 = order.company.address.address2
  billTo.locality = order.company.address.district
  billTo.administrativeArea = order.company.address.state
  billTo.postalCode = order.company.address.code
  billTo.country = 'BR'

  const instrument = new cybersourceRestApi.Tmsv1paymentinstrumentsInstrumentIdentifier()
  instrument.card = { number: card.number }

  body2.card = _.omit(card, 'number')
  body2.billTo = billTo
  body2.instrumentIdentifier = instrument

  return new Promise(function (resolve, reject) {
    instance.tmsV1PaymentinstrumentsPost(app.config.cyberSource.profile, body2, function (error, data, response) {
      if (error) {
        reject(error)
      }
      if (data) {
        resolve(data)
      }
    })
  })
}