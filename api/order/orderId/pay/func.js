const app = require('@/app')
const errors = require('@/errors')

module.exports = async(context, res) => {
  const { orderId } = context.params
  const { card } = context.body

  const Order = app.models.order
  const order = await Order.findOne({ _id: orderId }).populate('company')

  if(!order) {
    throw new errors.NotFound('order')
  }

  if(order.finished || !order.isReady) {
    throw new errors.BadRequest('Wrong state')
  }

  let resp = await app.helpers.pay(order, card)

  // update order with returned status
  // TODO, deal with errors
  order.finished = true
  order.payment.id = resp.id
  order.payment.status = resp.status

  return await order.save()
}