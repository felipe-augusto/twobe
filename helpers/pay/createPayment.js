const app = require('@/app')
const cybersourceRestApi = require('cybersource-rest-client')

module.exports = async function createPayment(order, paymentId) {
  const instance = new cybersourceRestApi.PaymentsApi(app.config.cyberSource.config)
  const clientReferenceInformation = new cybersourceRestApi.Ptsv2paymentsClientReferenceInformation()
  clientReferenceInformation.code = 'test_payment'

  const processingInformation = new cybersourceRestApi.Ptsv2paymentsProcessingInformation()
  processingInformation.commerceIndicator = 'internet'

  const amountDetails = new cybersourceRestApi.Ptsv2paymentsOrderInformationAmountDetails()
  amountDetails.totalAmount = order.total.amount
  amountDetails.currency = 'BRL'

  const orderInformation = new cybersourceRestApi.Ptsv2paymentsOrderInformation()
  orderInformation.amountDetails = amountDetails

  const paymentInformation = new cybersourceRestApi.Ptsv2paymentsPaymentInformation()
  paymentInformation.customer = new cybersourceRestApi.Ptsv2paymentsPaymentInformationCustomer()
  paymentInformation.customer.customerId = paymentId

  const request = new cybersourceRestApi.CreatePaymentRequest()
  request.paymentInformation =  paymentInformation
  request.clientReferenceInformation = clientReferenceInformation
  request.processingInformation = processingInformation
  request.orderInformation = orderInformation

  // enable automatic capture
  request.processingInformation.capture = true

  return new Promise(function (resolve, reject) {
    instance.createPayment(request, function (error, data, response) {
      if (error) {
        reject(error)
      }
      else if (data) {
        resolve(data)
      }
    })
  })
}