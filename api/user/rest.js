const app = require('@/app')
const restify = require('express-restify-mongoose')

restify.serve(app.router, app.models.user, {
  prefix: '',
  version: '',
  access(req) {
    return 'protected'
  },
  writeAccess(req) {
    return 'protected'
  },
  lean: { virtuals: true },
  totalCountHeader: true,
  runValidators: true,
  preRead: [app.helpers.rest.paginate],
  // preCreate: [
  //   guard.check('orders:write'),
  //   app.helpers.rest.appendBodyAuthor
  // ],
  // preUpdate: guard.check('orders:write'),
  // preDelete: guard.check('orders:write'),
  outputFn: app.helpers.rest.outputFn
})
