const _ = require('lodash')

module.exports = (amount, currency = 'BRL' ) => {
  if(!_.isNumber(amount)) {
    throw Error('Amount must be a number')
  }
  currency = currency.toUpperCase()
  const methods = {
    BRL(amount = 0) {
      amount = amount.toFixed(2).split('.')
      amount[0] = `R$ ${amount[0].split(/(?=(?:...)*$)/).join('.')}`
      return amount.join(',')
    }
  }
  return methods[currency] ? methods[currency](amount) : methods.BRL(amount)
}
