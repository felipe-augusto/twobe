const app = require('@/app')
const errors = require('@/errors')

const priceDisplay = app.helpers.amount.display

const Schema = require('mongoose').Schema

const Model = module.exports = new Schema({
  // Product name
  name: {
    type: String,
    required: true
  },

  // Vital statistics
  measure: {
    quantity: {
      type: String,
      required: true
    },
    kind: {
      type: String,
      required: true
    }
  },

  // Categories that this product is linked to
  categoriesIds: {
    type: [String],
    required: true
  },

  // Product description
  description: {
    type: String,
    required: true
  },

  price: {
    amount: {
      type: Number,
      default: 0
    },
    display: {
      type: String,
      default: null
    }
  }
})

Model.virtual('categories', {
  ref: 'categories',
  localField: 'categoriesIds',
  foreignField: '_id',
  justOne: false
})

Model.pre('save', async function(){
  // Verify if this measure kind exists
  let Measure = app.models.measure
  if(!((await Measure.distinct('name')).includes(this.measure.kind))) {
    // throw new errors.BadRequest(`This measure dont exist: ${this.measure.kind}`)
  }

  this.price.display = priceDisplay(this.price.amount)
})
